package controllers

import javax.inject.Inject

import model.CurrenciesResponse
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, Controller}
import services.ForeignExchangeService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by Alexander on 08.05.2016.
  */
class ForeignExchangeController @Inject()(wsClient: WSClient,
                                          foreignExchangeService: ForeignExchangeService) extends Controller {
  def postLatestData() = Action.async {
    wsClient.url("http://api.fixer.io/latest").get().map { response =>
      response.status match {
        case play.api.http.Status.OK =>
          foreignExchangeService.batchPersist(getData(response.body))
          Ok("OK")
        case _ => InternalServerError
      }
    }
  }

  private def getData(content: String): Seq[(String, Double)] = {
    val pattern = "\"(\\S{3}?)\":(\\d+\\.?\\d*)".r
    pattern.findAllMatchIn(content).toSeq.collect {
      case a if (a.groupCount == 2) => (a.group(1), a.group(2).toDouble)
    }
  }

  def getLatesDate() = Action.async {
    import model.CurrenciesResponseConversions._

    Future {
      foreignExchangeService.getLatest()
    }.map { r =>
      Ok(Json.toJson(CurrenciesResponse(r)))
    }
  }


}
