package services

import javax.inject.{Inject, Singleton}

import dao.CurrencyDao
import model.Currency

/**
  * Created by Alexander on 08.05.2016.
  */
@Singleton
class ForeignExchangeService @Inject() (currencyDao: CurrencyDao) {
  def batchPersist(data: Seq[(String, Double)]): Unit = {
    currencyDao.batchPersist(data)
  }

  def getLatest(): List[Currency] = {
    currencyDao.getLatest()
  }
}
