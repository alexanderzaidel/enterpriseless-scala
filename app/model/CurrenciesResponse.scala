package model

import play.api.libs.json.Json

/**
  * Created by Alexander on 08.05.2016.
  */
case class CurrenciesResponse(currencies: List[Currency])

object CurrenciesResponseConversions {

  import model.CurrencyConvertions.currencyWrites

  implicit val resp = Json.writes[CurrenciesResponse]
}