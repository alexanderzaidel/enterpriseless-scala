package dao

import javax.inject.{Inject, Singleton}

import model.Currency
import play.api.Configuration
import scalikejdbc._
/**
  * Created by Alexander on 08.05.2016.
  */
@Singleton
class CurrencyDao @Inject() (val config: Configuration) extends BaseDao {

  def batchPersist(data: Seq[(String, Double)]): Unit = DB autoCommit { implicit session =>
    val batchParams = data.map {d => Seq(d._1, d._2)}
    sql"insert into currencyScala(currency, created, rate) values(?, now(), ?)".batch(batchParams: _*).apply()
  }

  def getLatest(): List[Currency] = DB readOnly { implicit session =>
    sql"""select
            c.currency,
            c.created,
            c.rate
          from currencyScala c
            where
              c.id in (select max(c1.id) from currencyScala c1 group by c1.currency)
       """. map { rs =>
      Currency(None, rs.jodaDateTime("created"), rs.string("currency"), rs.float("rate"))
    }.list().apply()
  }
}
