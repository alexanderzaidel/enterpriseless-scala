package dao

import play.api.Configuration
import scalikejdbc.ConnectionPool

/**
  * Created by Alexander on 08.05.2016.
  */
trait BaseDao {
  def config: Configuration

  private lazy val url = getProperty("enterpriseless.scala.db.url")
  private lazy val user = getProperty("enterpriseless.scala.db.username")
  private lazy val password = getProperty("enterpriseless.scala.db.pasword")

  Class.forName("org.postgresql.Driver")
  ConnectionPool.singleton(url, user, password)

  private def getProperty(name: String): String = config.getString(name).getOrElse(throw new IllegalArgumentException(s"${name} wasn't found"))
}
