name := """enterpriseless-scala"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "org.scalikejdbc" % "scalikejdbc_2.11" % "2.4.0.RC2",
  "org.scalikejdbc" % "scalikejdbc-interpolation_2.11" % "2.4.0.RC2",
  "org.scalikejdbc" % "scalikejdbc-core_2.11" % "2.4.0.RC2",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
